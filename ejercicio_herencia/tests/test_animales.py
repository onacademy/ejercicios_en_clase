from app.animales import Perro, Paloma, Persona, Contador

def test_animales():
    perro = Perro()
    assert perro.cantidad_de_patas() == 4
    assert perro.ladrar() == "guau"

    paloma = Paloma()
    assert paloma.cantidad_de_patas() == 2
    assert paloma.volar() == "mueve las alas"

    persona = Persona()
    assert persona.cantidad_de_patas() == 2
    assert persona.hablar() == "bla"

    lista_de_animales = []
    lista_de_animales.append(persona)
    lista_de_animales.append(paloma)
    lista_de_animales.append(perro)

    assert Contador.contar_patas_de(lista_de_animales) == 8
