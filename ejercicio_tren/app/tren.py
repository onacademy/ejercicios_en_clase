class Vagon:

    def __init__(self, capacidad_pasajeros_sentados, capacidad_pasajeros_parados):
        if (capacidad_pasajeros_parados <= 0 or capacidad_pasajeros_sentados <= 0):
            raise ValueError("La cantidad de pasajeros debe ser positiva")
        self.__capacidad_pasajeros_sentados = capacidad_pasajeros_sentados
        self.__capacidad_pasajeros_parados = capacidad_pasajeros_parados

    def capacidad(self):
        return self.__capacidad_pasajeros_sentados + self.__capacidad_pasajeros_parados

class Locomotora:

    def __init__(self, potencia):
        if (potencia != 1000 and potencia != 2000):
            raise PotenciaInvalidaError("La potencia debe ser 1000 o 2000")
        self.__potencia = potencia

    def potencia(self):
        return self.__potencia

    def cantidad_max_vagones(self):
        if self.__potencia == 1000:
            return 4
        return 7

class Tren:

    def __init__(self, locomotora):
        self.__locomotora = locomotora
        self.__vagones = []

    def agregar(self, vagon):
        if (len(self.__vagones) == self.__locomotora.cantidad_max_vagones()):
            raise DemasiadosVagonesError("La locomotora no soporta mas")
        self.__vagones.append(vagon)

    def capacidad(self):
        capacidad_total = 0
        for vagon in self.__vagones:
            capacidad_total += vagon.capacidad()
        return capacidad_total

class PotenciaInvalidaError(RuntimeError):
    pass

class DemasiadosVagonesError(RuntimeError):
    pass
