from datetime import datetime
from flask import Flask, render_template, request


app = Flask(__name__)


@app.route('/')
def index():
    return render_template("inicio.html")


@app.route('/calculadora')
def calculadora():
    accion = request.args.get('para', 'sumar')
    return render_template("calculadora.html", accion=accion, mensaje="")


@app.route('/sumar', methods=['Post'])
def sumar():
    try:
        x = int(request.form['x'])
        y = int(request.form['y'])
        resultado = Calculadora().sumar(x, y)
        return render_template("resultado.html", x=x, y=y, accion='+', resultado=resultado)
    except OperacionNoValida:
        return render_template("calculadora.html", accion='sumar', mensaje="No podes hacer eso, proba otra vez"), 400
    except:
        return render_template("resultado.html", x=x, y=y, accion='+', resultado="Error inesperado"), 500

@app.route('/restar', methods=['Post'])
def restar():
    try:
        x = int(request.form['x'])
        y = int(request.form['y'])
        resultado = Calculadora().restar(x, y)
        return render_template("resultado.html", x=x, y=y, accion='-', resultado=resultado)
    except OperacionNoValida:
        return render_template("calculadora.html", accion='restar', mensaje="No podes hacer eso, proba otra vez"), 400
    except:
        return render_template("resultado.html", x=x, y=y, accion='-', resultado="Error inesperado"), 500

class Calculadora:

    def sumar(self,x,y):
        if x == 0:
            raise OperacionNoValida()
        return x + y

    def restar(self,x,y):
        return x - y

class OperacionNoValida(RuntimeError):
    pass
