from app.cola_sobre_pila import ColaSobrePila, Pila
import pdb

def test_pila_se_crea_vacia():
    assert Pila().esta_vacia() == True

def test_pila_desapila_el_ultimo_aplilado():
    pila = Pila()
    pila.apilar(1)
    pila.apilar(2)
    pila.apilar(3)

    assert pila.desapilar() == 3
    assert pila.desapilar() == 2
    assert pila.desapilar() == 1


def test_pila_desapila_none_cuando_esta_vacia():
    pila = Pila()
    assert pila.desapilar() == None


def test_pila_no_esta_vacia_cuando_apilo():
    pila = Pila()
    pila.apilar(1)
    assert pila.esta_vacia() is False


def test_cola_se_crea_vacia():
    assert ColaSobrePila().esta_vacia() is True


def test_cola_desencola_el_primero_encolado():
    cola = ColaSobrePila()
    cola.encolar(1)
    cola.encolar(2)
    cola.encolar(3)

    assert cola.desencolar() == 1
    assert cola.desencolar() == 2
    assert cola.desencolar() == 3

def test_cola_desencola_none_cuando_esta_vacia():
    cola = ColaSobrePila()
    assert cola.desencolar() == None

def test_cola_no_esta_vacia_cuando_encolo():
    cola = ColaSobrePila()
    cola.encolar(1)
    assert cola.esta_vacia() == False
