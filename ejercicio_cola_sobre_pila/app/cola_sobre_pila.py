import pdb

class Pila:
    def __init__(self):
        self.pila = []

    def apilar(self, valor):
        self.pila.append(valor)

    def desapilar(self):
        if not self.pila:
            return None
        else:
            return self.pila.pop(-1)

    def esta_vacia(self):
        return len(self.pila) == 0

    def __bool__(self):
        return not self.esta_vacia() # bool(self.pila)

    def __len__(self):
        return len(self.pila)

class ColaSobrePila:
    """Una cola puede ser implementada sobre una pila de forma muy elegante.
    Para encolar, simplemente agrego un valor a la pila (apilo)
    Para desencolar, uso una pila auxiliar para invertir el orden de la pila y saco el primero
    """
    def __init__(self):
        self.pila_interna = Pila()

    def esta_vacia(self):
        return self.pila_interna.esta_vacia()

    def encolar(self, valor):
        self.pila_interna.apilar(valor)

    def desencolar(self):
        # Creo pila auxiliar para invertir el orden y sacar el valor de mas abajo de la pila
        pila_invertida = Pila()
        # Por Ej: si la pila_interna tiene
        # | 3 |
        # | 2 |
        # | 1 |
        # La auxiliar va a invertir el orden quedando asi
        # | 1 |
        # | 2 |
        # | 3 |
        # De esta forma, el primer valor en ingresar, queda mas arriba y lo puedo obtener con un pop
        if self.pila_interna.esta_vacia():
            return None
        # Mientras haya valores en la pila
        while not self.pila_interna.esta_vacia():
            # Sacando los elementos de una pila y apilandolos en otra, invierto el orden de la pila
            pila_invertida.apilar(self.pila_interna.desapilar())
        # El valor de mas arriba de la pila invertida
        # Es el valor de mas abajo de la pila interna
        # Es decir, el primer valor ingresado
        # En una cola, el primer valor ingresado es el que debe salir
        # Este es el 'truco' de la cola sobre pilas
        valor_a_desencolar = pila_invertida.desapilar()
        # Vuelvo a popular mi pila interna respetando el orden original
        # Desapilar una pila e ir metiendola en otra es invertir el orden de la pila
        while not pila_invertida.esta_vacia():
            self.pila_interna.apilar(pila_invertida.desapilar())
        return valor_a_desencolar
