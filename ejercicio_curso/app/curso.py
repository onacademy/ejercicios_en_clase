from datetime import date

class Estudiante:

    def __init__(self, nombre, dni, fecha_nacimiento, secundario_completo):
        self.nombre = nombre
        self.dni = dni
        self.fecha_nacimiento = fecha_nacimiento
        self.secundario_completo = secundario_completo

    def es_mayor_de_edad(self):
        edad = date.today().year - self.fecha_nacimiento.year
        return edad >= 18

    def nacido_en(self, anio):
        return self.fecha_nacimiento.year == anio

    def __eq__(self, otro):
        return self.nombre == otro.nombre and self.dni ==  otro.dni

    def es_misma_persona_que(self, otro):
        return self.nombre == otro.nombre and self.dni ==  otro.dni

class Curso:

    def __init__(self, cupo):
        self.cupo = cupo
        self.estudiantes = []
        self.nombre = "sin nombre"

    def __str__(self):
        return "Objeto curso con cupo {}".format(self.cupo)


    def __len__(self):
        return self.cupo


    def agregar_estudiante(self, estudiante):
        if estudiante.es_mayor_de_edad() and self.cantidad_de_estudiantes() < self.cupo:
            self.estudiantes.append(estudiante)
        else:
            raise ValueError("El estudiante es menor o cupo lleno")

    # code smell
    def cantidad_de_estudiantes_nacidos_en(self, anio):
        contador = 0
        for estudiante in self.estudiantes:
            if estudiante.nacido_en(2000):
                contador += 1
        return contador


    def cantidad_de_estudiantes(self):
        return len(self.estudiantes)
