from app.sala import *


def test_sala_de_espera_con_varios_pacientes():
    sala_de_espera = SalaDeEspera()
    sala_de_espera.llega_nuevo_paciente("juan")
    sala_de_espera.llega_nuevo_paciente("maria")
    sala_de_espera.llega_nuevo_paciente("ana")

    assert sala_de_espera.siguiente_paciente() == "juan"
    assert sala_de_espera.siguiente_paciente() == "maria"
    assert sala_de_espera.siguiente_paciente() == "ana"


def test_sala_de_espera_se_crea_vacia():
    assert SalaDeEspera().siguiente_paciente() == None

def test_sala_de_espera_un_paciente():
    sala_de_espera = SalaDeEspera()
    sala_de_espera.llega_nuevo_paciente("juan")
    assert sala_de_espera.siguiente_paciente() == "juan"


def test_cola_se_crea_vacia():
    assert Cola().esta_vacia() == True

def test_cola_desencola_el_primero_encolado():
    cola = Cola()
    cola.encolar(1)
    cola.encolar(2)
    cola.encolar(3)

    assert cola.desencolar() == 1
    assert cola.desencolar() == 2
    assert cola.desencolar() == 3

def test_cola_desencola_none_cuando_esta_vacia():
    cola = Cola()
    assert cola.desencolar() == None

def test_cola_no_esta_vacia_cuando_encolo():
    cola = Cola()
    cola.encolar(1)
    assert cola.esta_vacia() == False

def test_pila_se_crea_vacia():
    assert Pila().esta_vacia() == True

def test_pila_desapila_el_ultimo_aplilado():
    pila = Pila()
    pila.apilar(1)
    pila.apilar(2)
    pila.apilar(3)

    assert pila.desapilar() == 3
    assert pila.desapilar() == 2
    assert pila.desapilar() == 1

    #assert pila.esta_vacia() == True

def test_pila_desapila_none_cuando_esta_vacia():
    pila = Pila()
    assert pila.desapilar() == None

def test_pila_no_esta_vacia_cuando_apilo():
    pila = Pila()
    pila.apilar(1)
    assert pila.esta_vacia() == False
