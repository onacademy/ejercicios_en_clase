#!/bin/bash
# test.sh

set -e

function check() {
  python3 app.py "$2" "$3" "$4" "$5" | grep "$6" >/dev/null && echo "$1:ok" || echo "$1:error"
}


# python app.py <fecha_alquiler> <cuit> <tipo_alquiler> <parametros_alquiler>

check '01-alquiler por hora' 20190119 20112223336 h 3 'Importe: 300'
check '02-alquier por dia' 20190119 20112223336 d 1 'Importe: 2000'
check '03-alquier por dia empresa' 20190119 26112223336 d 1 'Importe: 1900'
#check '04-alquier miercoles' 20190821 20112223336 h 3 'Importe: 250'