from app.api import api
import json


def test_get():
    response = api.test_client().post('/sumar',
     data=json.dumps({'x': 1,'y': 2}),
     content_type='application/json',
    )
    assert response.status_code == 200
    assert response.json['resultado'] == 3
