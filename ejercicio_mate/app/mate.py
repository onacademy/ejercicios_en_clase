class Mate:

    def __init__(self, cebadas_disponibles):
        self.__esta_vacio = True
        self.__cebadas_disponibles = cebadas_disponibles

    def esta_vacio(self):
        return self.__esta_vacio

    def esta_lleno(self):
        return not self.esta_vacio()

    def cebadas_disponibles(self):
        return self.__cebadas_disponibles

    def cebar(self):
        if (self.esta_lleno()):
            raise ValueError("No se puede cebar si esta lleno")
        if (self.__cebadas_disponibles > 0):
            self.__cebadas_disponibles = self.__cebadas_disponibles - 1
        else:
            print("Mate lavado!!!!")
        self.__esta_vacio = False


    def beber(self):
        if (self.esta_vacio()):
            raise ValueError("No se puede beber si esta vacio")
        self.__esta_vacio = True
